  #ifndef _PRODUCT_H_
  #define _PRODUCT_H_
/*
product.h
Mirza Mohammad Faiq Baig
Created:21/01/2021
Updated:21/01/2021
*/
class Product {
    private:

    int id;
    int stock;
    int totalStock;
    float cost;

    public:
    Product(int id, int stock, int totalStock, float cost); //Passing variables through constructors

    void setID (int id);
    void setStock (int stock);
    void setTotalStock (int totalStock);
    void setCost (float cost);

    int getID();
    int getStock();
    int getTotalStock();
    float getCost();
};
#endif