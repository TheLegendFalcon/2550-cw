#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include "CSV.h"
/*
 CSV.h
Mirza Mohammad Faiq Baig
 Created: 22/1/2021
 Updated: 22/1/2021
*/
CSV::CSV(std::string filename, std::string delm) : 
  fileName(filename), delimeter(delm), linesCount(0)
  {}
  std::string CSV::GetFile(){
      return this -> fileName;
  }

    std::vector<std::vector<std::string>> CSV::getData()
      {
        std::ifstream file(fileName);
        std::vector<std::vector<std::string> > dataList;
        std::string line, word;  
        if(!file)
            std::cout<<"File is Empty ";
        else  
        while(getline(file, line))
        {
        std::vector<std::string> vec;
        std::stringstream ss(line);
        }
        while (getline(ss, word,','))
            vec.push_back(word);
    
        dataList.push_back(vec);
      }
      file.close();
      return dataList;
  }