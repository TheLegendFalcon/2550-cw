#include<string>
#include<vector>
#include<iostream>
#include"DVD.h"
#include"cd.h"
#include"Product.h"
/*
 DVD.cpp
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
*/
DVD::DVD(std::string title, int year, int id,
int stock, int totalstock, float cost) : Product(id, cost, stock, totalstock)
{
    this -> title = title;
    this -> year = year;
}

void DVD::setYear(int year)
{
    this -> year = year;
}

void DVD::setTitle(std::string title)
{
    this -> title = title;
}

int DVD::getYear()
{
    return year;
}

std::string DVD::getTitle()
{
    return title;
}

std::vector<std::string> DVD::Display()
{
    std::cout << "\n\n" << std::string(90, '-') << "\n";
    std::cout << "|";
    return {"ID", "Title", "Year", "Cost", "Stock", "Total Stock"};
}

std::vector<std::string> DVD::List(){
    return {
        std::to_string(getID()), getTitle(), std::to_string(getYear()), std::to_string(getCost()), 
        std::to_string(getStock()), std::to_string(getTotalStock())
    };
}