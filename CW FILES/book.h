#ifndef _BOOK_H_
#define _BOOK_H_
#include "Product.h"
/*
 Book.h
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
*/

class Book : public Product {
    private:
    std::string author;
    std::string title;
    int ISBN;
    int year;

    public:
    Book(std::string title, std::string author, int ISBN, int year,
    int id, int stock, int totalStock, float cost);
    Book();

    void setISBN(int ISBN);
    void setYear(int year);
    void setAuthor(std::string author);
    void setTitle(std::string title);

    int getISBN();
    int getYear();
    std::string getAuthor();
    std::string getTitle();

    std::vector<std::string> List();
    std::vector<std::string> Display();
};
#endif