#ifndef _DVD_H_
#define _DVD_H_
#include "Product.h"
/*
 DVD.h
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
*/

class DVD : public Product {
    private:
    std::string title;
    int year;

    public:
    DVD(std::string title, int year, int id, int stock,
    int totalStock, float cost);
    
    void setYear(int year);
    void setTitle(std::string title);

    int getYear();

    std::string getTitle();
    std::vector<std::string> List();
    std::vector<std::string> Display();
};
#endif