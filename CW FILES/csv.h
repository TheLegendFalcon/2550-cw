#ifndef _CSV_H_
#define _CSV_H_
/*
 CSV.h
 Mirza Mohammad Faiq Baig
 Created: 22/1/2021
 Updated: 22/1/2021
*/
class CSV{
    private:
    std::string fileName;
    std::string delimeter;
    int linesCount;

    public:

    CSV(std::string filename, std::string delm = ",");
    //Memeber function for storing comma separated value
    template<typename T>
    void addData(T first, T last)
    {
        std::fstream file;

        file.open(fileName, std::ios::out | std::ios::app);

        for (;first != last;)
        {
            file << *first;

            if (++first != last)
            file << delimeter;
        }

        file << "\n";
        linesCount++; 
        //Close file
        file.close();
    }
    std::string GetFile();
    //Read data
    std::vector<std::vector<std::string>> getData();
};
#endif