#include <string>
#include <iostream>
#include <vector>
#include "magazine.h"
#include "Book.h"
#include "Product.h"
/*
 Book.cpp
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
*/
Book::Book(std::string title, std::string author, int ISBN, int year,
    int id, int stock, int totalStock, float cost) : Product(id, cost, stock, totalStock)

{
    this -> title = title;
    this -> author = author;
    this -> year = year;
    this -> ISBN = ISBN;
}

void Book::setYear(int year)
{
    this -> year = year;
}

void Book::setISBN(int ISBN)
{
    this -> ISBN = ISBN;
}

void Book::setTitle(std::string title)
{
    this -> title = title;
}

void Book::setAuthor(std::string author)
{
    this -> author = author;
}

int Book::getYear()
{
    return year;
}

int Book::getISBN()
{
    return ISBN;
}

std::string Book::getTitle()
{
    return title;
}

std::string Book::getAuthor()
{
    return author;
}

std::vector<std::string> Book::Display()
{
    std::cout << "\n\n" << std::string(105, '-') << "\n";
    std::cout << "|";
    return {"ID", "Title", "Author", "Year", "Cost", "Stock", "Total Stock"};
}

std::vector<std::string> Book::List()
{
    return {std::to_string(getID()), getTitle(), getAuthor(),std::to_string(getYear()),
          std::to_string(getISBN()), std::to_string(getCost()), 
          std::to_string(getStock()), std::to_string(getTotalStock()) };
}