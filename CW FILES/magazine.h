#ifndef _MAGAZINE_H_
#define _MAGAZINE_H_
#include <string>
#include "Book.h"
/*
 Magazine.h
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
*/

class Magazine : public Book{
    private:
    int issue;

    public:
    Magazine();
    Magazine(std::string title, std::string author, int ISBN, int issue,
    int year, int id, int stock, int totalStock, float cost);

    void setIssue(int issue);

    std::vector<std::string> Display();
    std::vector<std::string> List();

    int getIssue();
};
#endif