#include <string>
#include "Product.h"
/*
product.cpp
Mirza Mohammad Faiq Baig
Created:21/01/2021
Updated:21/01/2021
*/
Product::Product(int id, int stock, int totalStock, float cost)

{
    this -> id = id;
    this -> stock = stock;
    this -> totalStock = totalStock;
    this -> cost = cost;
}

void Product::setID(int id)
{
    this->id = id;
}
void Product::setStock(int stock)
{
    this->stock = stock;
}
void Product::setTotalStock(int totalStock)
{
    this->totalStock = totalStock;
}
void Product::setCost(float cost)
{
    this->cost = cost;
}

int Product::getID()
{
    return this->id;
}
int Product::getStock()
{
    return this->stock;
}
int Product::getTotalStock()
{
    return this->totalStock;
}
float Product::getCost()
{
    return this->cost;
}