#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <fstream>
#include "Product.h"
#include "DVD.h"
#include "CD.h"
#include "Book.h"
#include "Magazine.h"
#include "CSV.h"
/*
main.cpp
Mirza Mohammad Faiq Baig
Created:21/01/2021
Updated:21/01/2021
*/
/*----------------------------------Menu Function----------------------------------*/

void Select(int);
void SellItem();
void Getdata(std::string);
void WriteData(std::string);
void ReadData(std::string);
void UpdateStock(std::string);

void menu()
{
  int x;

	std::cout<<"\n\n\t\t\t========================= Welcome to Stock Management System portal ============================\n\n";
	std::cout << "\n\t\t\t Please choose what you want\n";
	std::cout << "\t\t\t1. Sell item\n";
	std::cout << "\t\t\t2. Restock\n";
	std::cout << "\t\t\t3. Add new item\n";
	std::cout << "\t\t\t4. Update stock\n";
	std::cout << "\t\t\t5. View Report\n";
	std::cout << "\t\t\t6. Exit the portal\n";
	std::cout << "\n\n\t-->> Enter your choice only number..\n\t--> ";
	std::cin >> x;

	switch(x){

		case 1:
		Select(x);
		break;
		
		case 2:
		Select(x);
		break;

		case 3:
		Select(x);
		break;

		case 4:
		Select(x);
		break;

		case 5:
		Select(x);
		break;

		case 6:
		Select(x);
		break;

		case 7:
		break;

		default :
		std::cout << "\n\n\t\t\tYou have entered a wrong choice. Please try again";
		menu();
		break;
		}
	}

void Select(int function)
{
	int choice;
	std::cout << "\n\n\n\t\t\t################ Please select type ################";
	std::cout << "\n\n\n\t\t\t\t 1. CD " << std::endl;
	std::cout << "\n\n\n\t\t\t\t 2. DVD " << std::endl;
	std::cout << "\n\n\n\t\t\t\t 3. Magazine " << std::endl;
	std::cout << "\n\n\n\t\t\t\t 4. Book " << std::endl;
	std::cout << "\n\n\n\t\t\t\t 5. Go Back " << std::endl;
	std::cout << "\n\n\n\t\t\t\t Enter your choice: ";
	std::cin >> choice;

	switch (choice)
	{
		case 1:
		if (function == 1) Getdata("CD");
		else if(function == 3) UpdateStock("CD");
		else if(function == 6) ReadData("CD");
		menu();
		break;

		case 2:
		if (function == 1) Getdata("DVD");
		else if(function == 3) UpdateStock("DVD");
		else if(function == 6) ReadData("DVD");

		case 3:
		if (function == 1) Getdata("Magazine");
		else if(function == 3) UpdateStock("Magazine");
		else if(function == 6) ReadData("Magazine");

		case 4:
		if (function == 1) Getdata("Book");
		else if(function == 3) UpdateStock("Book");
		else if(function == 6) ReadData("Book");

		case 5:
		ReadData("All");
		break;

		case 0:
		menu();
		break;
		default :

		std::cout << "\n\n\n\t\t\t You have entered a wrong choice, please enter again ";
		system("clear");
		Select(function);
		break;
	}
}

void Getdata(std::string type)
{
  std::string artist, title, author;
  int year, id, stock, totalstock, ISBN, issue;
  float cost;

  std::cout<<"\n\n\n\t\t\t\t Enter Product id: ";
  std::cin >> id;
  if (type == "CD" || type == "DVD")
  {
	  std::cout << "\t\t\t Enter Title: ";
	  std::cin >> title;
	  if (type == "CD"){
		  std::cout << "\t\t\t Enter Artist: ";
		  std::cin >> artist;}
		std::cout << "\t\t\t Enter Year: ";
		std::cin >> year;
	  }
	  else if ( type == "Magazine" || type == "Book" )
	  {
		 std::cout << "\t\t\t Enter Title: ";
		 std::cin >> title;
		 
		 std::cout << "\t\t\t Enter Author: ";
		 std::cin >> author;

		 if ( type == "Magazine"){
			 std::cout << "\t\t\t Enter Issue: ";
			 std::cin >> issue;}

			 std::cout << "\t\t\t Enter ISBN: ";
			 std::cin >> ISBN;

			 std::cout << "\t\t\t Enter Year:";
			 std::cin >> year;
		 }

		 std::cout << "\t\t\t Enter cost: ";
		 std::cin >> cost;

		 stock:
		 	std::cout<<"\t\t\t\t Enter Current Stock: ";
    		std::cin>>stock;
    		std::cout<<"\t\t\t\t Enter Total Stock: ";
    		std::cin>>totalstock;
		if ( totalstock < stock){
    		std::cout<<"\n\t\t\t\t Totalstock Can't Be Lower Than Stock."<<std::endl;
    		goto stock; }
  			if ( type == "CD") c = CD(title, artist, year, id, cost, stock, totalstock);
  			else if ( type == "DVD") d = DVD(title, year, id, cost, stock, totalstock);
  			else if ( type == "Magazine") m = Magazine(title, author, ISBN, issue, year, id, cost, stock, totalstock);
  			else if ( type == "Book") b = Book(title, author, ISBN, year, id, cost, stock, totalstock); 
  		WriteData(type);
  		std::cout<<"\n\n\t\t\t\t Data has been saved to file."<<std::endl;
	  }

void WriteData(std::string type)
{
	std::vector<std::string> dataList;
	dataList.clear();
	fwrite = CSV(type + ".csv");
	
	if(type == "CD")     
		dataList = { c.List() };
	else if(type == "DVD")
		dataList = { d.List() };
	else if(type == "Mag")
		dataList = { m.List() };
	else if(type == "Book")
		dataList = { b.List() };
	
	write.addData(dataList.begin(), dataList.end());    
}	