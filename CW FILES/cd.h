#ifndef _CD_H_
#define _CD_H_
#include "DVD.h"
/*
 CD.h
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
*/

class CD : public DVD{

    private:
    std::string artist;

    public:
    CD(std::string artist, std::string title, int year, int id,
    int stock, int totalStock, float cost);

    void setArtist(std::string artist);

    std::string getArtist();
    std::vector<std::string> Display();
    std::vector<std::string> List();
};
#endif