#include <string>
#include <vector>
#include <iostream>
#include "Magazine.h"
#include "Book.h"
/*
 Magazine.cpp
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
 */

Magazine::Magazine(std::string title, std::string author, int ISBN, int issue,
int year, int id, int stock, int totalStock, float cost) : Book(title, author, ISBN, year, id, stock, totalStock, cost)

{
    this -> issue = issue;
}
void Magazine::setIssue(int issue)
{
    this -> issue = issue;
}
int Magazine::getIssue()
{
    return issue;
}

std::vector<std::string> Magazine::Display()
{
    std::cout << "\n\n" << std::string(120, '-') << "\n";
    std::cout << "|";

    return {"ID", "Title", "Author", "Issue", "Year", "Cost", "Stock", "Total Stock"};
}

std::vector<std::string> Magazine::List()
{
    return {std::to_string(getID()), getTitle(), getAuthor(), std::to_string(getIssue()), std::to_string(getYear()), std::to_string(getISBN()), std::to_string(getCost()), std::to_string(getStock()), std::to_string(getTotalStock()) };
}