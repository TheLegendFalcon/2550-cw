#include <string>
#include <iostream>
#include <vector>
#include <iomanip>
#include "cd.h"
#include "dvd.h"
/*
 CD.cpp
 Mirza Mohammad Faiq Baig
 Created: 21/1/2021
 Updated: 21/1/2021
*/
CD::CD(std::string artist, std::string title, int year, int id,
    int stock, int totalStock, float cost) : DVD(title, year, id, cost, stock, totalStock)
{
    this -> artist = artist;
}

void CD::setArtist(std::string artist)
{
    this -> artist = artist;
}

std::string CD::getArtist()
{
    return artist;
}
std::vector<std::string> CD::Display()
{
  std::cout <<"\n\n" <<std::string(105, '-') << "\n";   
  std::cout<<"|";
  return{"PID", "Title", "Artist", "Year", "Cost", "Stock", "Total Stock"};
}

std::vector<std::string> CD::List()
{
  return {std::to_string(getID()), getTitle(), getArtist(), 
          std::to_string(getYear()), std::to_string(getCost()), 
          std::to_string(getStock()), std::to_string(getTotalStock()) 
          };
}